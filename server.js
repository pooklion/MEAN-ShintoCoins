const express = require('express');
let app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());
let path = require('path');
app.use(express.static(path.join(__dirname, 'shintoApp', 'dist')));
app.all('*', (req,res,next) => {
    res.sendFile(path.join(__dirname, 'shintoapp','dist','index.html'))
});

app.listen(8000, ()=>{
    console.log('Shinto on 8000');
});